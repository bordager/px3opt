'''
A simple script that demonstrates the basic usage of px3opt
'''
from px3opt.Material import *
from px3opt.Core import Session
from px3opt.Source import SpeckPySource, SourceFromSpectrum
from px3opt.Detector import Detector

matI = MaterialByElement('I', 0.2, 1)
matBa = MaterialByElement('Ba', 0.3,1)

Lth_range = np.arange(25,45)
Hth_range = np.arange(25,45)
src_list = [SpeckPySource(U, None, th=90, z=28.12, mas=0.23) for U in [50,70,80] ]

pixieIII = Detector()

S = Session(matI,matBa,src_list,pixieIII, Lth_range, Hth_range)
S.compute()
S.showMaps( ['CNR_KES'])