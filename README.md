# px3opt

px3opt, standing for PixieIII OPTimization, is an optimization tool for acquisition with PixieIII Xray detector written in python.
It is based on modeling of the sample contrast and empirical parametrization of the detector behavior.

An article is in preparation that described this developments and evaluates the model.

***

## Documentation

Documentation for installation and usage can be found at https://tomox_simap.gricad-pages.univ-grenoble-alpes.fr/px3opt

## Roadmap
Future developments would includes:

Technical developments:
- GUI interface for basic usage
- save function for saving result of computations as a structured folder on disk.
- representation of the different objects by xml files, associated with load and save functions

Scientific developments:
- Taking into account efficiency and linearity of detector

## Contact 
Please contact remi.granger@univ-grenoble-alpes.fr for any request regarding this software.

## License
This package is free to use, distribute and adapt for non-commercial use only. See [LICENSE](https://gricad-gitlab.univ-grenoble-alpes.fr/TomoX_SIMaP/px3opt/-/blob/main/LICENSE) for license rights and limitations (CC BY-NC 4.0).

## Reference
Publication attached to this software can be foud at https://doi.org/10.48550/arXiv.2207.14673 .

Please cite the following reference when using this software:

GRANGER, Rémi, SALVO, Luc, ROSCOAT, Sabine Rolland du, et al. Estimation of the optimal parameters for K-edge subtraction imaging with PixiRad-2/PixieIII photon counting detector on a conventional laboratory X-ray micro-tomograph. arXiv preprint arXiv:2207.14673, 2022.