Getting started
===============

Here is an example of basic usage for optimizing contrast in a sample composed of Iodine at 0.2g/cm^3 ans Barium at 0.3g/cm^3,
searching for optimal points for thresholds between 25 keV and 45keV and source tension at 50 kV, 70 kV and 80 kV.

First, after importing appropriate classes, create the :py:mod:`Material` objects that represents the two material to be contrasted:

.. code-block:: python

    from px3opt.Material import *
    from px3opt.Core import Session
    from px3opt.Source import SpeckPySource, SourceFromSpectrum
    from px3opt.Detector import Detector

    matI = MaterialByElement('I', 0.2, 1)
    matBa = MaterialByElement('Ba', 0.3,1)

Then create the search domain for optimization: ranges for threshold and a list of :py:mod:`Source` objects, storing the different source configurations you want to explore.
In the src_list, you can mix instances of any class from the :py:mod:`Source` module.

.. code-block:: python

    Lth_range = np.arange(25,45)
    Hth_range = np.arange(25,45)
    src_list = [SpeckPySource(U, None, E_lim=[16,48], th=90, z=28.12, mas=0.23) for U in [50,70,80] ]

Then create the :py:mod:`Detector` object:

.. code-block:: python

    pixieIII = Detector()

Initialize an optimization :py:class:`~Core.Session` with these inputs and run the computations:

.. code-block:: python

    S = Session(matI,matBa,src_list,pixieIII, Lth_range, Hth_range)
    S.compute()

Finally, display the result of interest:

.. code-block:: python

   S.showMaps( ['CNR_KES'])
