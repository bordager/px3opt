px3opt.Source.py
===============================

.. automodule:: Source
    :members:
    :undoc-members:
    :private-members:
    :show-inheritance:
    :no-show-inheritance: _GenericSource
