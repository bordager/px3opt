Download & Installation
=======================

Requierments
------------

The package's depedencies are:

From classical python distribution:

* matplotlib (tested with v3.4.2)
* numpy (tested with v1.21.2)
* scipy (tested with v1.6.3)
* re (teted with v2.2.1)

Specific packages :

* `xraylib <https://github.com/tschoonj/xraylib/wiki>`_ ( tested with v4.1.1)
* `spekpy <https://bitbucket.org/spekpy/spekpy_release/wiki/Home>`_ (tested with v2.0.6)

Installation
------------

Ones the previous dependencies are satisfied, one can install the package by donwloading `git repository <https://gricad-gitlab.univ-grenoble-alpes.fr/TomoX_SIMaP/px3opt.git>`_ and placing it  in the PYTHONPATH.
