import numpy as np
import spekpy as sp

from px3opt.Material import _GenericMaterial, MaterialByElement

class _GenericSource:
    '''
    Generic class, that holds the necessary attributes and method to comply with the :py:class:`Core.Session` class.
    '''
    def __init__(self):
        self.Erange = None
        self.I0 = None
        self.label = None

    def add_filter(self, material):
        '''
        update the source psectrum by taking into account  absorption from a filter define by material

        :param material: material describing the absorbance spectra of the filter
        :type material: :py:mod:`Material`
        '''
        assert issubclass(type(material), _GenericMaterial)

        if (material.Erange.min() > self.Erange.min() or material.Erange.max() < self.Erange.max()):
            raise ValueError("Source Erange should be included in filter Erange")

        A = np.interp(self.Erange, material.Erange, material.A)

        self.I0 *= np.exp(-A)

class SourceFromSpectrum(_GenericSource):
    '''
    Creates a source from spectrum defined by energy bins and associated counts per pixel

        :param Erange: 1D array that describe energy bins
        :param I0: Same shape as Erange, describes the number of counts per pixel for each energy bin
        :param label: a label to Identify the source
    '''
    def __init__(self, Erange, I0, label):
        super().__init__()
        self.Erange = Erange
        self.I0 = I0
        self.label=label

class SpeckPySource(_GenericSource):
    '''
    :param float or int U: Source tension
    :param list filter: 2 element list [str, int] passed to spekpy's .filter() method
    :param label: a label to identify the source config, if not given will be generated from arguments
    :type label: str
    :param float or int E_lim: lim to restrict generated Erange
    :param kwargs: keywords arguments passed to SpekPy's sp.Spek() method
    '''
    def __init__(self, U,filter=None,label=None,E_lim=None, **kwargs):
        super().__init__()
        self.U = U
        self.filter = filter
        self.kwargs = kwargs
        if label is None:
            self.label = '__'.join(['%dkV'%(U)]+[key +'_'+ str(kwargs[key]) for key in kwargs])
        else:
            self.label = label

        s = sp.Spek(kvp=U,**kwargs)
        if filter is not None: s.filter(*filter)

        Erange, I0 = s.get_spectrum(edges=True)
        self.Erange = Erange[::2]
        self.I0 = I0[::2]
        if E_lim is not None:
            mask = (E_lim[0] < self.Erange)* (self.Erange<E_lim[1])
            self.Erange=self.Erange[mask]
            self.I0 = self.I0[mask]

if __name__ =='__main__':
    import matplotlib.pyplot as plt

    src = SpeckPySource(50, th =90, physics='casim', z=30, mas=2, targ='W')
    filter = MaterialByElement('Cu', 1, Erange=src.Erange)

    fig = plt.figure()
    plt.suptitle(src.label)
    plt.plot(src.Erange, src.I0, label='no filter')
    src.add_filter(filter)
    plt.plot(src.Erange, src.I0, label='filter')
    plt.show()