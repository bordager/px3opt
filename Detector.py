class Detector:
    '''
    Object that represents the detecotr used in the simulation.

    :param float or int exposure: Exposure time (s)
    :param float spectral_res: Spectral resolution of the detector (keV), defined as the kernel of gaussian smoothing of the spectra.
    :param float px_size: Pixel size of the detector (µm)
    '''
    def __init__(self, exposure=1, spectral_res=1.8,px_size=62):
        self.exposure = exposure
        self.spectral_res = spectral_res
        self.px_size = px_size