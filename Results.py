from px3opt.Functions import *


class resultMaps():
    '''
    Holds different estimated quantities as function of the input parameters. The quantities are stored as Ns x Nl xNh numpy array where
    Ns  = len(session.src_list),  Nl=session.Lth_range.size, Nh=session.Hth_range.size.

    :param session: The sesssion from wich the result will be obtained.
    :type session: :py:class:`~Core.Session`

    :ivar CNR_KES: CNR for KES modality
    :ivar CNR_ABS: CNR for ABS modality
    :ivar C_KES: Contrast for KES
    :ivar C_ABS: Contrast for ABS
    :ivar I_0_1: Input flux withou sample (mean count level on flat) for the low energy register
    :ivar I_0_2: Input flux withou sample (mean count level on flat) for the high energy register
    :ivar mult: multiplicative factor for the input flux to get the prescribed FL count level (see :py:class:`~Core.Session`))
    :ivar best_CNR: maximum of ABS_CNR and KES_CNR
    :ivar modality: modality to use for best performances
    '''
    def __init__(self, session):
        self.CNR_KES = np.empty([len(session.src_list), session.Lth_range.size, session.Hth_range.size], dtype=np.float32)
        self.CNR_ABS = np.empty_like(self.CNR_KES)
        self.C_KES = np.empty_like(self.CNR_KES)
        self.C_ABS = np.empty_like(self.CNR_KES)
        self.sigma_KES = np.empty_like(self.CNR_KES)
        self.sigma_ABS = np.empty_like(self.CNR_KES)
        self.I_0_1 = np.empty_like(self.CNR_KES)
        self.I_0_2 = np.empty_like(self.CNR_KES)
        self.mult = np.empty_like(self.CNR_KES)
        self.bestCNR = np.empty_like(self.CNR_KES)
        self.modality = np.empty_like(self.CNR_KES, dtype=np.int8)
        self.arePresent = False

class resultsOptima:
    '''
    Stores the optima as lists [optimal source label, optimal L_th, optimal H_th]

    :ivar Global_optimum: The global optimum covering both KES and ABS modalities
    :ivar KES_optimum: Optimum for KES modality
    :ivar ABS_optimum: Optimum for ABS modality
    :ivar KES_optimum_mapcoord: coordinates of the CNR map optimum for KES
    :ivar ABS_optimum_mapcoord: coordinates of the CNR map optimum for ABS
    '''
    def __init__(self):
        self.Global_optimum = None
        self.KES_optimum = None
        self.ABS_optimum = None
        self.KES_optimum_mapCoord = None
        self.ABS_optimum_mapCoord = None


class Results:
    '''
    Stores output of the computations
    '''
    def __init__(self, session):
        self.optima_found = False
        self.session = session
        self.maps = resultMaps(self.session)
        self.optima = resultsOptima()

    def findOptima(self):
        '''
        Find optima from the result maps, and update the :py:class:`~Results.resultsOptima` object.
        '''
        self.optima.KES_optimum_mapCoord = np.unravel_index(np.nanargmax(self.maps.CNR_KES), self.maps.CNR_KES.shape)
        self.optima.KES_optimum = [self.session.src_list[self.optima.KES_optimum_mapCoord[0]].label,
                                   self.session.Lth_range[self.optima.KES_optimum_mapCoord[1]],
                                   self.session.Hth_range[self.optima.KES_optimum_mapCoord[2]]]

        self.optima.ABS_optimum_mapCoord = np.unravel_index(np.nanargmax(self.maps.CNR_ABS), self.maps.CNR_ABS.shape)
        self.optima.ABS_optimum = [self.session.src_list[self.optima.ABS_optimum_mapCoord[0]].label,
                                   self.session.Lth_range[self.optima.ABS_optimum_mapCoord[1]],
                                   self.session.Hth_range[self.optima.ABS_optimum_mapCoord[2]]]

        id = np.argmax([self.maps.CNR_KES[self.optima.KES_optimum_mapCoord],
                       self.maps.CNR_ABS[self.optima.ABS_optimum_mapCoord]])

        if id == 0:
            self.optima.Global_optimum = ['KES'] + self.optima.KES_optimum
        else:
            self.optima.Global_optimum =['ABS'] + self.optima.ABS_optimum

        self.optima_found = True